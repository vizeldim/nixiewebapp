ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.2.1"

//libraryDependencies += "com.raquo" %%% "domtypes" % "17.0.0-M4"
libraryDependencies += "io.monix" %%% "monix" % "3.4.1"
libraryDependencies += "com.lihaoyi" %%% "scalatags" % "0.12.0"
libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "2.1.0"


lazy val root = (project in file("."))
  .enablePlugins(ScalaJSPlugin)
  .settings(
    name := "NixieWebApp",
    idePackagePrefix := Some("cz.cvut.fit.psl.vizeldim"),
    scalaJSUseMainModuleInitializer := true,
    // ECMAScript
    scalaJSLinkerConfig ~= {
      _.withModuleKind(ModuleKind.ESModule)
    },
      // CommonJS
      scalaJSLinkerConfig ~= {
      _.withModuleKind(ModuleKind.CommonJSModule)
    }
  )
