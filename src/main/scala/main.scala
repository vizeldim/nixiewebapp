package cz.cvut.fit.psl.vizeldim

import ble.Characteristic
import ble.impl.*
import data.impl.GattProfile
import data.impl.domain.DataViewConverterExtensions.toClockDateTime
import data.impl.services.*
import data.layer.domain.ConnectionStatus.{Connected, Connecting, Disconnected}
import data.layer.domain.{ClockDate, ClockDateTime, ClockTime}
import data.layer.services.{AlarmService, ConnectionService, DateTimeService, StopWatchService, TimerService}
import data.stub.services.{TestAlarmService, TestConnectionService, TestDateTimeService, TestStopWatchService, TestTimerService}
import presentation.features.alarms.{AlarmsView, AlarmsViewModel}
import presentation.features.connection.{ConnectionView, ConnectionViewModel}
import presentation.features.datetime.{DateTimeView, DateTimeViewModel}
import presentation.features.stopWatch.{StopWatchView, StopWatchViewModel}
import presentation.features.timer.{TimerView, TimerViewModel}
import presentation.utils.Router

import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom
import org.scalajs.dom.{Event, document, webgl}
import scalatags.JsDom.all.*

import scala.concurrent.ExecutionContext.Implicits.global
import scala.scalajs.js
import scala.scalajs.js.typedarray.DataView
import scala.util.{Failure, Success}
import cz.cvut.fit.psl.vizeldim.presentation.components.CircularLoader


@main
def main(): Unit = {
  val TEST_MODE = false

  val root = document.createElement("div")
  root.id = "root"

  val content = document.createElement("div")
  content.id = "content"

  val nav = document.createElement("div")
  nav.id = "nav"

  root.appendChild(content)
  root.appendChild(nav)
  document.body.appendChild(root)

  val bleClient = ScJsBleClient()

  val testServices = Services(
    connection = TestConnectionService(),
    dateTime = TestDateTimeService(),
    alarm = TestAlarmService(),
    timer = TestTimerService(),
    stopWatch = TestStopWatchService()
  )

  val bleServices = Services(
    connection = BleConnectionService(bleClient),
    dateTime = BleDateTimeService(bleClient),
    alarm = BleAlarmService(bleClient),
    timer = BleTimerService(bleClient),
    stopWatch = BleStopWatchService(bleClient)
  )

  val services = if(TEST_MODE) { testServices }
                  else { bleServices }

  val connectionView = ConnectionView(ConnectionViewModel(services.connection))
  connectionView.initialize()

  val dateTimeView = DateTimeView(DateTimeViewModel(services.dateTime))
  val stopWatchView = StopWatchView(StopWatchViewModel(services.stopWatch))
  val timerView = TimerView(TimerViewModel(services.timer))
  val alarmsView = AlarmsView(AlarmsViewModel(services.alarm))

  val pages = Map(
    //    "index" -> ConnectionView(ConnectionViewModel(connectionService)),
    "datetime" -> dateTimeView,
    "alarms" -> alarmsView,
    "timer" -> timerView,
    "stopwatch" -> stopWatchView
  )

  val navLinks = Map(
    "datetime" -> "Date & Time",
    "alarms" -> "Alarms",
    "timer" -> "Timer",
    "stopwatch" -> "StopWatch"
  )


  val router = Router(content, nav, pages, navLinks)


  services.connection.streamConnectionStatus().foreach {
    case Connected => connected()
    case Disconnected => disconnected()
    case Connecting => loader()
  }


  def connected(): Unit = {
    dateTimeView.initialize()
    stopWatchView.initialize()
    timerView.initialize()
    alarmsView.initialize()

    router.showNav()
    router.navigate("datetime")
  }


  def disconnected(): Unit = {
    content.innerHTML = ""
    content.appendChild(connectionView.root)
    router.hideNav()
  }

  def loader(): Unit = {
    content.innerHTML = ""
    content.appendChild(
      div(cls := "loader-page") (
        CircularLoader().root
      ).render
    )
  }
}



case class Services(connection: ConnectionService,
                    dateTime: DateTimeService,
                    alarm: AlarmService,
                    timer: TimerService,
                    stopWatch: StopWatchService)