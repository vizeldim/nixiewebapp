package cz.cvut.fit.psl.vizeldim
package ble

import scala.concurrent.Future
import scala.scalajs.js.typedarray.DataView

trait BleClient {
  def connect(deviceId: String, onDisconnect: String => Unit): Future[Boolean]
  def requestDevice(serviceUUIDs: Array[String] = Array(), optionalServiceUUIDs: Array[String] = Array()): Future[BleDevice]
  def read(characteristic: Characteristic): Future[DataView]
  def write(characteristic: Characteristic, value: DataView): Future[Boolean]
  def startNotifications(characteristic: Characteristic, callback: DataView => Unit): Future[Boolean]
  def stopNotifications(characteristic: Characteristic): Future[Boolean]
}

case class BleDevice(deviceId: String, name: String)

case class Characteristic(serviceUUID: String, characteristicUUID: String)

