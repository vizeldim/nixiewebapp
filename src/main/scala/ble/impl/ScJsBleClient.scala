package cz.cvut.fit.psl.vizeldim
package ble.impl

import ble.{BleClient, BleDevice, Characteristic}

import org.scalajs.dom.Event

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}
import concurrent.ExecutionContext.Implicits.global
import scalajs.js
import js.JSConverters.*
import scala.scalajs.js.typedarray.DataView

class ScJsBleClient extends BleClient {
  private var device: Option[BluetoothDevice] = None

  def initialize(): Future[Boolean] = {
    val isAvailableF = Bluetooth.getAvailability().toFuture
    isAvailableF.map {
      isAvailable =>
        if (isAvailable) {
          true
        } else {
          throw Exception("Web Bluetooth API is not available in this browser.")
        }
    }
  }


  private def getScanOptions(serviceUUIDs: Array[String] = Array(), optionalServiceUUIDs: Array[String] = Array()): RequestDeviceOptions = {
    if (serviceUUIDs.isEmpty && optionalServiceUUIDs.isEmpty) {
      val allDevicesOption = RequestDeviceOptions(acceptAllDevices = true)
      return allDevicesOption
    }

    val filterDevicesByUUIDsOption = RequestDeviceOptions(
      filters = js.Array(BluetoothLEScanFilter(
        services = serviceUUIDs.toJSArray,
      )),
      optionalServices = optionalServiceUUIDs.toJSArray
    )

    filterDevicesByUUIDsOption
  }

  override def requestDevice(serviceUUIDs: Array[String] = Array(), optionalServiceUUIDs: Array[String] = Array()): Future[BleDevice] = {
    val scanOptions = getScanOptions(serviceUUIDs, optionalServiceUUIDs)

    Bluetooth
      .requestDevice(scanOptions)
      .toFuture
      .map { apiDevice =>
        device = Option(apiDevice)
        BleDevice(deviceId = apiDevice.id, name = apiDevice.name)
      }
  }


  override def connect(deviceId: String, onDisconnect: String => Unit): Future[Boolean] = {
    device.get.gatt.connect().toFuture.map { _ => true }
  }

  private def getApiCharacteristic(characteristic: Characteristic): Future[BluetoothRemoteGATTCharacteristic] = {
    val service = device.get.gatt.getPrimaryService(characteristic.serviceUUID)

    service.toFuture.flatMap { s => s.getCharacteristic(characteristic.characteristicUUID).toFuture }
  }

  override def read(characteristic: Characteristic): Future[DataView] = {
    val apiCharacteristic = getApiCharacteristic(characteristic)

    val dF = apiCharacteristic.flatMap {
      c => c.readValue().toFuture
    }

    dF.onComplete {
      case Success(d) => println(s"RCVD DATA: ${d}")
      case Failure(exception) => println(s"FAILED ${exception.getMessage}")
    }
    dF
  }

  override def write(characteristic: Characteristic, value: DataView): Future[Boolean] = {
    val apiCharacteristic = getApiCharacteristic(characteristic)


    apiCharacteristic.flatMap {
      c => c.writeValueWithResponse(value).toFuture
    }.map { _ => true }
  }

  override def startNotifications(characteristic: Characteristic, callback: DataView => Unit): Future[Boolean] = {
    try {
      val apiCharacteristicF = getApiCharacteristic(characteristic)

      apiCharacteristicF.map { apiCharacteristic =>
        try {
          apiCharacteristic
            .startNotifications()
            .toFuture
            .onComplete {
              case Failure(exception) => println("SUBSCRIPTION FAILED :(")
              case Success(value) =>
                apiCharacteristic.addEventListener("characteristicvaluechanged", { _ => {
                  callback(apiCharacteristic.value)
                }
                })
            }
        } catch {
          case e: Exception => println(s"[startNotifications] - ${e.getMessage}")
        }
        true
      }
    } catch {
      case e: Exception => println(s"getApiCharacteristic FAIL ${e.getMessage}")
        Future(true)
    }

  }

  override def stopNotifications(characteristic: Characteristic): Future[Boolean] = {
    val apiCharacteristicF = getApiCharacteristic(characteristic)

    apiCharacteristicF.map {
      characteristic =>
        characteristic.stopNotifications()
        true
    }
  }
}