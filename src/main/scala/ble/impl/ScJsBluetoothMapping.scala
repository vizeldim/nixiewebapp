package cz.cvut.fit.psl.vizeldim
package ble.impl

import org.scalajs.dom.EventTarget

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobal, JSImport, JSName}
import scala.scalajs.js.typedarray.{ArrayBufferView, DataView}
import scala.scalajs.js.{undefined, |}
import org.scalajs.dom.Navigator as nav

@js.native
@JSGlobal("navigator.bluetooth")
object Bluetooth extends js.Object {
  def requestDevice(options: RequestDeviceOptions): js.Promise[BluetoothDevice] = js.native
  def getAvailability(): js.Promise[Boolean] = js.native
}


@js.native
@JSGlobal
class BluetoothDevice extends EventTarget {
  def id: String = js.native
  def name: String = js.native
  def gatt: BluetoothRemoteGATTServer = js.native
}

class RequestDeviceOptions(
    val acceptAllDevices: js.UndefOr[Boolean] = undefined,
    val filters: js.UndefOr[js.Array[BluetoothLEScanFilter]] = undefined,
    val optionalServices: js.UndefOr[js.Array[String]] = undefined
) extends js.Object {}


class BluetoothLEScanFilter(
  val name: js.UndefOr[String] = undefined,
  val services: js.UndefOr[js.Array[String]] = undefined
) extends js.Object {}

@js.native
trait BluetoothRemoteGATTServer extends EventTarget {
  def connect(): js.Promise[BluetoothRemoteGATTServer] = js.native
  def disconnect(): Unit = js.native
  def getPrimaryService(service: String): js.Promise[BluetoothRemoteGATTService] = js.native
}

@js.native
trait BluetoothRemoteGATTService extends EventTarget {
  def uuid: String = js.native
  def device: BluetoothDevice = js.native
  def getCharacteristic(characteristic: String): js.Promise[BluetoothRemoteGATTCharacteristic] = js.native
}

@js.native
trait BluetoothRemoteGATTCharacteristic extends EventTarget {
  val value: DataView = js.native
  def service: BluetoothRemoteGATTService = js.native
  def readValue(): js.Promise[DataView] = js.native
  def writeValueWithResponse(value: DataView): js.Promise[Unit] = js.native
  def startNotifications(): js.Promise[BluetoothRemoteGATTCharacteristic] = js.native
  def stopNotifications(): js.Promise[Unit] = js.native
}
