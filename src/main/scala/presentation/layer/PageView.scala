package cz.cvut.fit.psl.vizeldim
package presentation.layer

import presentation.utils.Router

import org.scalajs.dom.Element

trait PageView {
  val root: Element

  def onOpen(): Unit = {}

  def onClose(): Unit = {}
}
