package cz.cvut.fit.psl.vizeldim
package presentation.components
import org.scalajs.dom.Element
import scalatags.JsDom.all.*


class CircularLoader {
  val root: Element = div(cls := "loader") (div(cls := "circle")).render
}
