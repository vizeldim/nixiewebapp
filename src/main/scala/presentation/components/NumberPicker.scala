package cz.cvut.fit.psl.vizeldim
package presentation.components

import data.layer.domain.ClockTime

import org.scalajs.dom.Element
import scalatags.JsDom.all.*

class NumberPicker {
    private val hoursElem = input(`type` := "number", min := "0", max := "99", value := "0").render
    private val minutesElem = input(`type` := "number", min := "0", max := "59", value := "0").render
    private val secondsElem = input(`type` := "number", min := "0", max := "59", value := "0").render

    val root: Element = div(cls := "number-picker")(
      div(cls := "number-picker-item")(
        label("HOURS"),
        hoursElem
      ),
      span(" : "),
      div(cls := "number-picker-item")(
        label("MINUTES"),
        minutesElem
      ),
      span(" : "),
      div(cls := "number-picker-item")(
        label("SECONDS"),
        secondsElem
      )
    ).render

    def getPickedTime(): ClockTime = {
      val hours = hoursElem.value.toInt
      val minutes = minutesElem.value.toInt
      val seconds = secondsElem.value.toInt
      
      ClockTime(hour = hours, minute = minutes, seconds = seconds)
    }  
}
