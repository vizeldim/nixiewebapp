package cz.cvut.fit.psl.vizeldim
package presentation.components
import org.scalajs.dom.{Element, MouseEvent}
import org.scalajs.dom.html.Div
import scalatags.JsDom.all.*

class Dialog(content: Element, onSubmit: () => Unit) {
  val root: Div = div(
    cls := "blur-overlay context hidden",
    onclick := {(e: MouseEvent) => { close() }}) (
      div(cls := "context-card", onclick := { (e: MouseEvent) => { e.stopPropagation() } }) (
        content,
        div(cls := "context-btns")(
          button("Submit", onclick := {() => close(); onSubmit()}, cls := "context-btn context-submit-btn"),
          button("Close", onclick := { () => close() }, cls := "context-btn context-close-btn")
        )
      )
  ).render

  def open(): Unit = {
    root.classList.remove("hidden")
  }

  def close(): Unit = {
    root.classList.add("hidden")
  }
}

