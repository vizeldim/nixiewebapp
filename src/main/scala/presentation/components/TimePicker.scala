package cz.cvut.fit.psl.vizeldim
package presentation.components
import org.scalajs.dom.Element
import org.scalajs.dom.html.Input
import scalatags.JsDom.all.*
import scalatags.JsDom.tags2.*

class TimePicker() {
  private val timePicker = input(`type` := "time")(id := "appt", name := "appt", cls := "time-input", required := true).render
  private val lbl = label("Choose a time for alarm:").apply(`for` := "appt")

  def getTime: (Int, Int) = { // Hour, Minute
    val chosenTime = timePicker.value
    val vals = chosenTime.split(':').map(str => str.toInt)
    (vals(0), vals(1))
  }

  val root: Element = div(cls := "time-picker")(lbl, timePicker).render
}
