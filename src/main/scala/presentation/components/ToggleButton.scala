package cz.cvut.fit.psl.vizeldim
package presentation.components
import org.scalajs.dom.html.{Button, Div, Input, Label}
import org.scalajs.dom.{Element, Event, document}
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all.{label, *}

def createToggleButton: (Element, Element) = {
  val checkbox = input(`type` := "checkbox", cls := "checkbox").render
  val slider = span(cls := "slider round").render
  val switch = label(cls := "switch").render
  val btn = div(cls := "enable-switch").render

  switch.appendChild(checkbox)
  switch.appendChild(slider)
  btn.appendChild(switch)
  (btn, checkbox)
}
