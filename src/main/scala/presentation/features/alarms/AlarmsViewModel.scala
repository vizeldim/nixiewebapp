package cz.cvut.fit.psl.vizeldim
package presentation.features.alarms

import data.layer.domain.{Alarm, AlarmTime}
import data.layer.services.AlarmService
import data.stub.services.TestAlarmService

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.{BehaviorSubject, Var}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class AlarmsViewModel(private val alarmService: AlarmService = TestAlarmService()) {
  val alarms: Map[Int, Var[AlarmState]] = (0 to 9).map { id => Var(AlarmState(id, Some(Alarm(id, AlarmTime(0, 0), false)), isLoading = true)) }.zipWithIndex.map(_.swap).toMap

  def loadAlarms(): Unit = {
    for( (id, subject) <- alarms ) {
      subject := AlarmState(id, None, isLoading = true)

      alarmService.getById(id).onComplete {
        case Failure(_) => subject := AlarmState(alarmId = id, None, fail = true)
        case Success(value) => subject := AlarmState(alarmId = id, alarm = Some(value))
      }
    }
  }

  def toggleAlarm(alarmId: Int, isActive: Boolean): Unit = {
    val alarmSubject = alarms(alarmId)
    alarmSubject := alarmSubject().copy(isUpdating = true)

    alarmService
      .setStatus(alarmId, isActive)
      .onComplete {
        case Failure(_) => alarmSubject := alarmSubject().copy(isUpdating = false, fail = true)
        case Success(_) => alarmSubject := alarmSubject().copy(alarm = Some(alarmSubject().alarm.get.copy(
          isActive = isActive
        )),isUpdating = false)
      }
  }

  def setAlarmTime(alarmId: Int, time: (Int, Int)): Unit = {
    val alarmSubject = alarms(alarmId)
    alarmSubject := alarmSubject().copy(isUpdating = true)

    val alarmTime = AlarmTime(time._1, time._2)
    alarmService
      .setTime(alarmId, alarmTime)
      .onComplete {
        case Failure(_) => alarmSubject := alarmSubject().copy(isUpdating = false, fail = true)
        case Success(_) => alarmSubject := alarmSubject().copy(alarm = Some(alarmSubject().alarm.get.copy(
          time = alarmTime
        )), isUpdating = false)
      }
  }
}


case class AlarmState(alarmId: Int, alarm: Option[Alarm], isLoading: Boolean = false, isUpdating: Boolean = false, fail: Boolean = false)