package cz.cvut.fit.psl.vizeldim
package presentation.features.alarms

import data.layer.domain.{Alarm, AlarmTime}
import data.layer.services.AlarmService
import presentation.components.{CircularLoader, Dialog, TimePicker, createToggleButton}
import presentation.features.alarms.AlarmsViewModel
import presentation.layer.PageView

import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.html.{Div, Input}
import org.scalajs.dom.{Element, MouseEvent, document}
import scalatags.JsDom.all.*
import scalatags.JsDom.tags.{li, ul}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class AlarmsView(private val viewModel: AlarmsViewModel = AlarmsViewModel()) extends PageView {
  val root: Div = document.createElement("div").asInstanceOf[Div]
  private val alarmsUI: List[(Element, Element, Element)] = (0 to 9).map { alarmId =>
    val alarmItem = li(cls := "alarm").render

    val fragment = document.createElement("div")

    val alarmTimeButton = div(cls := "dowtime").render
    fragment.appendChild(alarmTimeButton)

    val (toggleButton, toggleButtonCheckBox) = createToggleButton
    toggleButtonCheckBox.asInstanceOf[Input].checked = false

    toggleButtonCheckBox.addEventListener("change", (e: org.scalajs.dom.Event) => {
      e.preventDefault()
      val value = e.target.asInstanceOf[Input].checked
      viewModel.toggleAlarm(alarmId, isActive = value)
    })


    alarmTimeButton.onclick = (e: MouseEvent) => {
      e.preventDefault()
      dialogs(alarmId).open()
    }

    fragment.appendChild(toggleButton)

    alarmItem.innerHTML = ""
    alarmItem.appendChild(fragment)

    val loader = div(cls := "alarm-loader-wrapper hidden")(CircularLoader().root).render
    alarmItem.appendChild(loader)

    alarmTimeButton.textContent = "00:00"

    (alarmItem, alarmTimeButton, loader)
  }.toList

  createUI()
  def initialize(): Unit = {

    for ((id, alarmStateSubject) <- viewModel.alarms) {
      alarmStateSubject.foreach {
        alarmState =>
          println(alarmState)
          if (alarmState.fail) showFail(id)
          else if (alarmState.isLoading) showLoading(id)
          else if (alarmState.isUpdating) showUpdating(id)
          else showLoaded(alarmState.alarm.get)
      }
    }

    viewModel.loadAlarms()
  }

  private def showFail(alarmId: Int): Unit = {
    val alarmItem = alarmsUI(alarmId)._1
    alarmItem.textContent = "FAIL..."
  }

  private def showLoading(alarmId: Int): Unit = {
    val loader = alarmsUI(alarmId)._3
    loader.classList.remove("hidden")
  }

  private def showUpdating(alarmId: Int): Unit = {
    val loader = alarmsUI(alarmId)._3
    loader.classList.remove("hidden")
  }

  private val dialogs = (0 to 9).map { alarmId =>
    val picker = TimePicker()
    
    val dialog = Dialog(picker.root, () => {
      try {
        val time = picker.getTime

        viewModel.setAlarmTime(alarmId, time)
      } catch {
        case e: Exception => println(s"Invalid time format: ${e.getMessage}")
      }
    })

    document.body.appendChild(dialog.root)

    dialog
  }.toList



  private def showLoaded(alarm: Alarm): Unit = {
    val alarmUI = alarmsUI(alarm.id)

    val loader = alarmUI._3
    loader.classList.add("hidden")

    alarmUI._2.textContent = s"${"%02d".format(alarm.time.hour)}:${"%02d".format(alarm.time.minute)}"

    val alarmItem = alarmUI._1
    if (alarm.isActive) {
      alarmItem.classList.add("alarm-enabled")
    }
    else {
      alarmItem.classList.remove("alarm-enabled")
    }
  }

  private def createUI(): Unit = {
    val alarmsList = ul(id := "alarms-holder", cls := "alarms").render

    alarmsUI.foreach {
      alarmUI =>
        alarmsList.appendChild(alarmUI._1)
    }


    root.appendChild(alarmsList)
  }
}
