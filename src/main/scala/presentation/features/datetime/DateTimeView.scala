package cz.cvut.fit.psl.vizeldim
package presentation.features.datetime

import data.layer.domain.{ClockDate, ClockDateTime, ClockTime}
import presentation.components.CircularLoader
import presentation.features.datetime.DateTimeViewModel
import presentation.layer.PageView

import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.Element
import scalatags.JsDom.all.*

class DateTimeView(private val viewModel: DateTimeViewModel = DateTimeViewModel()) extends PageView {
  override val root: Element = div().render

  def initialize(): Unit = {
    viewModel.startStreamDateTime()

    viewModel.dateTimeStream.foreach {
      state =>
        if(state.isLoading) showLoading()
        else showDateTime(state.dateTime)
    }
  }

  private val timeElem = li(id := "time-now", cls := "time").render
  private val dateElem = li(id := "date-now", cls := "date").render
  private val dowElem = li(id := "dow-now", cls := "dow").render

  private val loader = div(cls := "loader-container hidden")(CircularLoader().root).render

  private val datetimeContent = div(cls := "datetime-inner")(
    div(cls := "datetime-card")(
      //          a(href := "#", cls := "edit-btn", id := "edit-btn"),
      ul(cls := "date-time-dow-list")(
        timeElem,
        li(cls := "date-dow")(
          ul(
            dateElem,
            dowElem,
            loader
          )
        )
      )
    )
  ).render

  createUI()

  def createUI(): Unit = {
    root.innerHTML = ""

    val container = div(id := "datetime-content", cls := "content")(
      datetimeContent
    ).render

    root.appendChild(container)
  }

  private def showDateTime(time: ClockDateTime): Unit = {
    dateElem.textContent = formatDate(time.date)
    timeElem.textContent = formatTime(time.time)
    loader.classList.add("hidden")
  }
  private def showLoading(): Unit = {
    timeElem.textContent = ""
    dateElem.textContent = ""
    loader.classList.remove("hidden")
  }

  private def formatDate(date: ClockDate): String = {
    val dayStr = "%02d".format(date.day)
    val monthStr = "%02d".format(date.month)
    val yearStr = "%04d".format(date.year)

    s"$dayStr/$monthStr/$yearStr"
  }

  private def formatTime(time: ClockTime): String = {
    val hourStr = "%02d".format(time.hour)
    val minuteStr = "%02d".format(time.minute)
    val secondsStr = "%02d".format(time.seconds)

    s"$hourStr:$minuteStr:$secondsStr"
  }

  override def onOpen(): Unit = {

  }
}
