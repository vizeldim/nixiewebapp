package cz.cvut.fit.psl.vizeldim
package presentation.features.datetime

import data.layer.domain.ClockDateTime
import data.layer.services.DateTimeService
import data.stub.services.TestDateTimeService

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.Var

class DateTimeViewModel(private val dateTimeService: DateTimeService = TestDateTimeService()) {
  val dateTimeStream: Var[DateTimeState] = Var(DateTimeState(isLoading = true))

  def startStreamDateTime(): Unit = {
    dateTimeStream := dateTimeStream().copy(isLoading = true)
    
    dateTimeService.streamDateTime.foreach { dateTime =>
      if(dateTime != ClockDateTime())
        dateTimeStream := dateTimeStream().copy(dateTime = dateTime, isLoading = false)
    }
  }
}

case class DateTimeState(dateTime: ClockDateTime = ClockDateTime(),
                         isLoading: Boolean = false,
                         isFail: Boolean = false)