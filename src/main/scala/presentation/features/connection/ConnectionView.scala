package cz.cvut.fit.psl.vizeldim
package presentation.features.connection

import presentation.features.connection.ConnectionViewState.{Initial, Scanning}
import presentation.layer.PageView

import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.{Element, MouseEvent}
import scalatags.JsDom.all.*

class ConnectionView(private val connectionViewModel: ConnectionViewModel = ConnectionViewModel()) extends PageView {
  override val root: Element = div().render
  
  private val connBtn = button("SCAN").render

  connBtn.onclick = (e: MouseEvent) => {
    e.preventDefault()
    connectionViewModel.scan()
  }
  
  root.append(
    div(cls := "scan-container")(
      connBtn
    ).render
  )

  def initialize(): Unit = {
    connectionViewModel.state.foreach {
      case Initial =>
        connBtn.textContent = "SCAN"
        connBtn.disabled = false
      case Scanning =>
        connBtn.textContent = "SCANNING"
        connBtn.disabled = true
    }
  }
}
