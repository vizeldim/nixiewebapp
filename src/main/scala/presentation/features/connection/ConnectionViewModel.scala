package cz.cvut.fit.psl.vizeldim
package presentation.features.connection

import data.layer.services.ConnectionService
import data.stub.services.TestConnectionService

import monix.reactive.subjects.Var

import scala.util.{Failure, Success}
import concurrent.ExecutionContext.Implicits.global
import monix.execution.Scheduler.Implicits.global
class ConnectionViewModel(private val connectionService: ConnectionService = TestConnectionService()) {
  val state: Var[ConnectionViewState] = Var(ConnectionViewState.Initial)
  
  def scan(): Unit = {
    state := ConnectionViewState.Scanning
    
    connectionService
      .scan()
      .onComplete {
        case Success(deviceId) =>
          connectionService.connect(deviceId)
        case Failure(_) =>
          state := ConnectionViewState.Initial
          println("SCAN ERROR :(")
      }
  }
}

enum ConnectionViewState {
  case Initial
  case Scanning
}