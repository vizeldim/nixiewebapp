package cz.cvut.fit.psl.vizeldim
package presentation.features.timer

import data.layer.domain.{StopWatchStatus, TimerStatus}
import data.layer.services.TimerService
import data.stub.services.TestTimerService

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.Var

import scala.util.{Failure, Success}

class TimerViewModel(private val timerService: TimerService = TestTimerService()) {
  val state: Var[TimerState] = Var(TimerState(isLoading = true))

  def startStreamSeconds(): Unit = {
    state := state().copy(isLoading = true)

    timerService.streamTime.foreach {
      seconds =>
        state := state().copy(seconds = seconds)
    }

    timerService.streamStatus.foreach {
      status =>
        state := state().copy(status = status)
    }

    timerService.readStatus().onComplete {
      case Success(status) => state := state().copy(status = status, isLoading = false)
    }
  }

  private def setStatus(status: TimerStatus): Unit = {
    timerService
      .setStatus(status)
      .onComplete {
        case Failure(_) => state := state().copy(isFail = true)
        case Success(_) => state := state().copy(status = status)
      }
  }

  def run(startTimeInSeconds: Int): Unit = {
    if(startTimeInSeconds < 1) {
      return
    }

    timerService
      .setStartTime(startTimeInSeconds)
      .onComplete {
        case Failure(_) => state := state().copy(isFail = true)
        case Success(_) => setStatus(TimerStatus.Running)
      }
  }

  def reset(): Unit = {
    setStatus(TimerStatus.Reset)
  }

  def pause(): Unit = {
    setStatus(TimerStatus.Paused)
  }

  def resume(): Unit = {
    setStatus(TimerStatus.Running)
  }

}

case class TimerState(seconds: Long = 0,
                      status: TimerStatus = TimerStatus.Reset,
                      isLoading: Boolean = false,
                      isFail: Boolean = false)