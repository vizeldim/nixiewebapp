package cz.cvut.fit.psl.vizeldim
package presentation.features.timer

import data.layer.domain.TimerStatus
import data.layer.domain.TimerStatus.{Paused, Reset, Running}
import presentation.components.NumberPicker
import presentation.layer.PageView

import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.Element
import scalatags.JsDom.tags.div
import presentation.components.CircularLoader

import cz.cvut.fit.psl.vizeldim.presentation.utils.formatSeconds
import scalatags.JsDom.all.*

class TimerView(private val viewModel: TimerViewModel = TimerViewModel()) extends PageView {
  val root: Element = div().render

  def initialize(): Unit = {
    viewModel.startStreamSeconds()

    viewModel.state.foreach {
      state =>
        if (state.isLoading) showLoading()
        else showTimer(state.seconds, state.status)
    }
  }

  private val timeContainer = div().render
  private val timePicker = NumberPicker()
  private val timeElem = h1(id := "time-now", cls := "time")("00:00:00").render

  private val controls = div(cls := "controls").render
  private val startBtn = button("START", cls := "control-btn", onclick := { () => handleRun() }).render
  private val resetBtn = button("RESET", cls := "control-btn control-btn-outline", onclick := { () => handleReset() }).render
  private val pauseBtn = button("PAUSE", cls := "control-btn", onclick := { () => handlePause() }).render
  private val resumeBtn = button("RESUME", cls := "control-btn", onclick := { () => handleResume() }).render

  private val loader = div(cls := "loader-container hidden")(CircularLoader().root).render

  private val timeContent = div(cls := "datetime-inner")(
    div(cls := "datetime-card stopwatch")(
      timeContainer,
      controls,
      loader
    )
  ).render

  createUI()

  def createUI(): Unit = {
    root.innerHTML = ""

    val container = div(id := "datetime-content", cls := "content")(
      timeContent
    ).render

    root.appendChild(container)
  }

  private var lastStatus: Option[TimerStatus] = None

  private def showTimer(timeInSeconds: Long, status: TimerStatus): Unit = {
    timeContainer.classList.remove("hidden")
    controls.classList.remove("hidden")
    loader.classList.add("hidden")

    if (lastStatus.isEmpty || lastStatus.get != status || status != TimerStatus.Reset) {
      showTime(timeInSeconds, status)
    }

    if (lastStatus.isEmpty || lastStatus.get != status) {
      showStatus(status)
    }
  }

  private def showStatus(status: TimerStatus): Unit = {
    lastStatus = Some(status)
    controls.innerHTML = ""

    status match
      case Paused =>
        controls.appendChild(resumeBtn)
        controls.appendChild(resetBtn)
      case Running =>
        controls.appendChild(pauseBtn)
        controls.appendChild(resetBtn)
      case Reset =>
        controls.appendChild(startBtn)
  }

  private def showTime(seconds: Long, status: TimerStatus): Unit = {
    timeContainer.innerHTML = ""

    if(status == TimerStatus.Reset) {
      timeContainer.appendChild(timePicker.root)
    }
    else {
      timeElem.textContent = formatSeconds(seconds)
      timeContainer.appendChild(timeElem)
    }
  }

  private def showLoading(): Unit = {
    timeContainer.classList.add("hidden")
    controls.classList.add("hidden")
    loader.classList.remove("hidden")
  }
  
  private def handleRun(): Unit = {
    val chosenTime = timePicker.getPickedTime()
    val timeInSeconds = (chosenTime.hour * 60 * 60) + (chosenTime.minute * 60) + (chosenTime.seconds)
    viewModel.run(timeInSeconds)
  }

  private def handlePause(): Unit = {
    viewModel.pause()
  }

  private def handleReset(): Unit = {
    viewModel.reset()
  }

  private def handleResume(): Unit = {
    viewModel.resume()
  }
}
