package cz.cvut.fit.psl.vizeldim
package presentation.features.stopWatch

import data.layer.domain.StopWatchStatus
import data.layer.domain.StopWatchStatus.{Paused, Reset, Running}
import presentation.components.CircularLoader
import presentation.layer.PageView
import presentation.utils.formatSeconds

import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.Element
import scalatags.JsDom.all.*
import scalatags.JsDom.tags.div

class StopWatchView(private val viewModel: StopWatchViewModel = StopWatchViewModel()) extends PageView {
  val root: Element = div().render

  def initialize(): Unit = {
    viewModel.startStreamSeconds()

    viewModel.state.foreach {
      state =>
        if (state.isLoading) showLoading()
        else showStopWatch(state.seconds, state.status)
    }
  }

  private val timeElem = h1(id := "time-now", cls := "time")("00:00:00").render
  private val controls = div(cls := "controls").render
  private val startBtn = button("START", cls := "control-btn", onclick := {() => handleRun()}).render
  private val resetBtn = button("RESET", cls := "control-btn control-btn-outline",  onclick := {() => handleReset()}).render
  private val pauseBtn = button("PAUSE", cls := "control-btn", onclick := {() => handlePause()}).render
  private val resumeBtn = button("RESUME", cls := "control-btn", onclick := {() => handleResume()}).render

  private val loader = div(cls := "loader-container hidden")(CircularLoader().root).render


  private val timeContent = div(cls := "datetime-inner")(
    div(cls := "datetime-card stopwatch")(
      timeElem,
      controls,
      loader
    )
  ).render

  createUI()

  def createUI(): Unit = {
    root.innerHTML = ""

    val container = div(id := "datetime-content", cls := "content")(
      timeContent
    ).render

    root.appendChild(container)
  }

  private var lastStatus: Option[StopWatchStatus] = None
  private def showStopWatch(timeInSeconds: Long, status: StopWatchStatus): Unit = {
    if (lastStatus.isEmpty || lastStatus.get != status) {
      timeElem.classList.remove("hidden")
      controls.classList.remove("hidden")
      loader.classList.add("hidden")

      showStatus(status)
    }

    showTime(timeInSeconds)
  }

  private def showStatus(status: StopWatchStatus): Unit = {
    lastStatus = Some(status)
    controls.innerHTML = ""

    status match
      case Paused =>
        controls.appendChild(resumeBtn)
        controls.appendChild(resetBtn)
      case Running =>
        controls.appendChild(pauseBtn)
        controls.appendChild(resetBtn)
      case Reset =>
        controls.appendChild(startBtn)
  }

  private def showTime(seconds: Long): Unit = {
    timeElem.textContent = formatSeconds(seconds)
  }

  private def showLoading(): Unit = {
    timeElem.classList.add("hidden")
    controls.classList.add("hidden")
    loader.classList.remove("hidden")
  }
  
  private def handleRun(): Unit = {
    viewModel.run()
  }

  private def handlePause(): Unit = {
    viewModel.pause()
  }

  private def handleReset(): Unit = {
    viewModel.reset()
  }

  private def handleResume(): Unit = {
    viewModel.run()
  }
}
