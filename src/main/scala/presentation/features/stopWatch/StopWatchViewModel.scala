package cz.cvut.fit.psl.vizeldim
package presentation.features.stopWatch

import data.layer.domain.StopWatchStatus
import data.layer.services.StopWatchService
import data.stub.services.TestStopWatchService

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.Var

import scala.util.{Failure, Success}

class StopWatchViewModel(private val stopWatchService: StopWatchService = TestStopWatchService()) {
  val state: Var[StopWatchState] = Var(StopWatchState(isLoading = true))

  def startStreamSeconds(): Unit = {
    state := state().copy(isLoading = true)

    stopWatchService.streamTime.foreach {
      seconds =>
        state := state().copy(seconds = seconds)
    }

    stopWatchService.getStatus.onComplete {
      case Failure(_) => state := state().copy(isFail = true)
      case Success(status) => state := state().copy(status = status, isLoading = false, isFail = false)
    }

  }

  private def setStatus(status: StopWatchStatus): Unit = {
    stopWatchService
      .setStatus(status)
      .onComplete {
        case Failure(_) => state := state().copy(isFail = true)
        case Success(_) => state := state().copy(status = status)
      }
  }

  def run(): Unit = {
    setStatus(StopWatchStatus.Running)
  }

  def reset(): Unit = {
    setStatus(StopWatchStatus.Reset)
  }

  def pause(): Unit = {
    setStatus(StopWatchStatus.Paused)
  }

}

case class StopWatchState(seconds: Long = 0,
                          status: StopWatchStatus = StopWatchStatus.Reset,
                          isLoading: Boolean = false,
                          isFail: Boolean = false)