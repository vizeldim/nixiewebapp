package cz.cvut.fit.psl.vizeldim
package presentation.utils


def formatSeconds(timeInSeconds: Long): String = {
  val seconds = timeInSeconds % 60
  val minutes = ((timeInSeconds - seconds) / 60) % 60
  val hours = ((((timeInSeconds - seconds) / 60) - minutes) / 60) % 60

  val hourStr = "%02d".format(hours)
  val minuteStr = "%02d".format(minutes)
  val secondsStr = "%02d".format(seconds)

  s"$hourStr:$minuteStr:$secondsStr"
}
