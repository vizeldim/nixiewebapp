package cz.cvut.fit.psl.vizeldim
package presentation.utils

import presentation.layer.PageView
import presentation.utils.NotFoundView

import org.scalajs.dom
import org.scalajs.dom.Element
import scalatags.JsDom.all.*


class Router(private val content: Element,
             private val nav: Element,
             private val pages: Map[String, PageView],
             private val navLinks: Map[String, String],
             private val initPageId: String = "index") {
  private val notFondPage = NotFoundView()

  private val navItems = navLinks.map { case (pageId, title) =>
    val item = li(cls := "nav-item", id := s"menu-$pageId").render
    val link = a(href := "#", onclick := { (e: dom.Event) =>
                  e.preventDefault()
                  navigate(pageId)
                })(title)

    item.appendChild(link.render)

    pageId -> item
  }

  nav.appendChild(createBottomNav())

  def navigate(pageId: String = initPageId): Unit = {
    val page = pages.getOrElse(pageId, notFondPage)

    content.innerHTML = ""
    content.appendChild(page.root)
    page.onOpen()
    clear()

    if(navItems.contains(pageId)) navItems(pageId).classList.add("nav-item-active")
  }

  private def createBottomNav(): Element = {
    val bottomNav = div(id := "bottom-nav")(
      div(cls := "nav")(
        ul(navItems.values.toSeq)
      )
    )
    bottomNav.render
  }

  private def clear(): Unit = {
    navItems.values.foreach {
      item => item.classList.remove("nav-item-active")
    }
  }

  def hideNav(): Unit = {
    nav.classList.add("hidden")
  }

  def showNav(): Unit = {
    nav.classList.remove("hidden")
  }
}
