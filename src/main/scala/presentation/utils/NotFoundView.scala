package cz.cvut.fit.psl.vizeldim
package presentation.utils

import presentation.layer.PageView

import org.scalajs.dom.Element
import scalatags.JsDom.all.*

class NotFoundView extends PageView {
  val root: Element = div(p("Page Not Found :(")).render

  override def onOpen(): Unit = {}

  override def onClose(): Unit = {}
}
