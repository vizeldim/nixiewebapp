package cz.cvut.fit.psl.vizeldim
package data.impl.domain

import data.layer.domain.{Alarm, AlarmTime, ClockDate, ClockDateTime, ClockTime, StopWatchStatus, TimerStatus}

import scala.scalajs.js
import scala.scalajs.js.typedarray.DataView
import scala.scalajs.js.typedarray.ArrayBuffer

object DataViewConverterExtensions {
  extension (clockDateTime: ClockDateTime) {
    def toDataView: DataView = {
      val buffer = ArrayBuffer(7)
      val dataView = DataView(buffer)

      dataView.setUint16(0, clockDateTime.date.year, true)
      dataView.setUint8(2, clockDateTime.date.month.toShort)
      dataView.setUint8(3, clockDateTime.date.day.toShort)
      dataView.setUint8(4, clockDateTime.time.hour.toShort)
      dataView.setUint8(5, clockDateTime.time.minute.toShort)
      dataView.setUint8(6, clockDateTime.time.seconds.toShort)

      dataView
    }
  }

  extension (boolean: Boolean) {
    def toDataView: DataView = {
      val buffer = ArrayBuffer(1)
      val dataView = DataView(buffer)
      val num: Short = if(boolean) 1 else 0

      dataView.setUint8(0, num)

      dataView
    }
  }

  extension (num: Short) {
    def toDataView: DataView = {
      val buffer = ArrayBuffer(1)
      val dataView = DataView(buffer)

      dataView.setUint8(0, num)

      dataView
    }
  }

  extension (num: Long) {
    def toDataView: DataView = {
      val buffer = ArrayBuffer(4)
      val dataView = DataView(buffer)

      dataView.setUint32(0, num)

      dataView
    }
  }

  extension (alarmTime: AlarmTime) {
    def toClockDateTime: ClockDateTime = {
      ClockDateTime(
        time = ClockTime(hour = alarmTime.hour, minute = alarmTime.minute)
      )
    }

    def toDataView: DataView = {
      alarmTime.toClockDateTime.toDataView
    }
  }

  extension (timerStatus: TimerStatus) {
    def toDataView: DataView = {
      timerStatus.ordinal.toShort.toDataView
    }
  }

  extension (stopWatchStatus: StopWatchStatus) {
    def toDataView: DataView = {
      stopWatchStatus.ordinal.toShort.toDataView
    }
  }

  extension (date: js.Date) {
    def toClockDateTime: ClockDateTime = {
      val second = date.getSeconds().toInt
      val minute = date.getMinutes().toInt
      val hour = date.getHours().toInt
      val day = date.getDate().toInt
      val month = date.getMonth().toInt + 1
      val year = date.getFullYear().toInt
      ClockDateTime(ClockTime(hour, minute, second), ClockDate(day, month, year))
    }
  }


  extension (dataView: DataView) {
    def toClockDateTime: ClockDateTime = {
      val year = dataView.getUint16(0, true)
      val month = dataView.getUint8(2).toInt
      val day = dataView.getUint8(3).toInt
      val hour = dataView.getUint8(4).toInt
      val minute = dataView.getUint8(5).toInt
      val second = dataView.getUint8(6).toInt

      ClockDateTime(ClockTime(hour, minute, second), ClockDate(day, month, year))
    }

    def toBoolean: Boolean = {
      val num = dataView.getUint8(0).toInt
      num == 1
    }

    def toShort: Short = {
      dataView.getUint8(0)
    }

    def toSeconds: Long = {
      dataView.getUint32(0).toLong
    }

    def toTimerStatus: TimerStatus = {
      val num = dataView.toShort
      TimerStatus.fromOrdinal(num)
    }

    def toStopWatchStatus: StopWatchStatus = {
      val num = dataView.toShort
      StopWatchStatus.fromOrdinal(num)
    }
    
    def toAlarmTime: AlarmTime = {
      val hour = dataView.getUint8(4).toInt
      val minute = dataView.getUint8(5).toInt

      AlarmTime(hour, minute)
    }
  }
}
