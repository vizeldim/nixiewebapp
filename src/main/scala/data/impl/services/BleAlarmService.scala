package cz.cvut.fit.psl.vizeldim
package data.impl.services
import ble.{BleClient, Characteristic}
import data.impl.GattProfile
import data.impl.domain.DataViewConverterExtensions.{toAlarmTime, toBoolean, toDataView}
import data.layer.domain.{Alarm, AlarmTime}
import data.layer.services.AlarmService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class BleAlarmService(private val bleClient: BleClient) extends AlarmService {
  private val uuids = GattProfile.ALARM_SERVICE_UUIDS.toArray
  private val timeCharacteristics = uuids.map{serviceUUID => Characteristic(serviceUUID, GattProfile.ALARM_TIME_CHARACTERISTIC) }
  private val statusCharacteristics = uuids.map{serviceUUID => Characteristic(serviceUUID, GattProfile.ALARM_STATUS_CHARACTERISTIC) }

  // ID: <0, 9>
  override def getById(id: Int): Future[Alarm] = {
    val alarmTime: Future[AlarmTime] = bleClient
      .read(timeCharacteristics(id))
      .map{ data => data.toAlarmTime }

    val isEnabled: Future[Boolean] = bleClient
      .read(statusCharacteristics(id))
      .map { data => data.toBoolean }

    val alarm = for {
      time <- alarmTime
      enabled <- isEnabled
    } yield Alarm(id, time, enabled)

    alarm
  }

  override def setStatus(id: Int, isActive: Boolean): Future[Boolean] = {
    bleClient.write(statusCharacteristics(id), isActive.toDataView)
  }

  override def setTime(id: Int, time: AlarmTime): Future[Boolean] = {
    bleClient.write(timeCharacteristics(id), time.toDataView)
  }
}
