package cz.cvut.fit.psl.vizeldim
package data.impl.services

import ble.{BleClient, Characteristic}
import data.impl.GattProfile
import data.impl.domain.DataViewConverterExtensions.*
import data.layer.domain.TimerStatus
import data.layer.services.TimerService

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.Var

import scala.concurrent.Future

class BleTimerService(private val bleClient: BleClient) extends TimerService {
  private val uuid = GattProfile.TIMER_SERVICE_UUID
  private val timeCharacteristic = Characteristic(uuid, GattProfile.TIMER_TIME_GETTER_CHARACTERISTIC)
  private val startTimeCharacteristic = Characteristic(uuid, GattProfile.TIMER_TIME_SETTER_CHARACTERISTIC)
  private val statusCharacteristic = Characteristic(uuid, GattProfile.TIMER_STATUS_CHARACTERISTIC)

  val timeStream: Var[Long] = Var(0)
  val statusStream: Var[TimerStatus] = Var(TimerStatus.Reset)


  def streamTime: Observable[Long] = {
    bleClient.startNotifications(
      timeCharacteristic,
      data =>
        timeStream := data.toSeconds
    )

    timeStream
  }

  def setStartTime(seconds: Long): Future[Boolean] = {
    bleClient
      .write(startTimeCharacteristic, seconds.toDataView)
      .map(_ => true)
  }

  override def streamStatus: Observable[TimerStatus] = {
    bleClient.startNotifications(
      statusCharacteristic,
      data =>
        statusStream := data.toTimerStatus
    )

    statusStream
  }

  override def setStatus(status: TimerStatus): Future[Boolean] = {
    bleClient
      .write(statusCharacteristic, status.toDataView)
      .map(_ => true)
  }

  override def readStatus(): Future[TimerStatus] = {
    bleClient
      .read(statusCharacteristic)
      .map(data => data.toTimerStatus)
  }
}
