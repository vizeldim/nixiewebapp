package cz.cvut.fit.psl.vizeldim
package data.impl.services

import ble.BleClient
import data.impl.GattProfile
import data.layer.domain.ConnectionStatus
import data.layer.services.ConnectionService

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.Var

import scala.concurrent.Future

class BleConnectionService(private val bleClient: BleClient) extends ConnectionService {
  private val connectionStatus: Var[ConnectionStatus] = Var(ConnectionStatus.Disconnected)

  private val serviceUUIDs = Array(GattProfile.CLOCK_SERVICE_UUID)

  private val optionalServiceUUIDs =
    Array(GattProfile.CLOCK_SERVICE_UUID,
          GattProfile.TIMER_SERVICE_UUID,
          GattProfile.STOPWATCH_SERVICE_UUID) ++
    GattProfile.ALARM_SERVICE_UUIDS.toArray

  // returns deviceId
  override def scan(): Future[String] = {
    bleClient
      .requestDevice(serviceUUIDs, optionalServiceUUIDs)
      .map(device => device.deviceId)
  }

  override def connect(deviceId: String): Future[Boolean] = {
    println("[connect]")
    connectionStatus := ConnectionStatus.Connecting
    bleClient.connect(deviceId = deviceId, onDisconnect = { _ =>
      connectionStatus := ConnectionStatus.Disconnected
    }).map(x =>
      connectionStatus := ConnectionStatus.Connected
      true
    )
  }

  override def streamConnectionStatus(): Observable[ConnectionStatus] = { connectionStatus }
}
