package cz.cvut.fit.psl.vizeldim
package data.impl.services

import ble.{BleClient, Characteristic}
import data.impl.GattProfile
import data.impl.domain.DataViewConverterExtensions.*
import data.layer.domain.StopWatchStatus
import data.layer.services.StopWatchService

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.Var

import scala.concurrent.Future

class BleStopWatchService(private val bleClient: BleClient) extends StopWatchService {
  private val uuid = GattProfile.STOPWATCH_SERVICE_UUID
  private val timeCharacteristic = Characteristic(uuid, GattProfile.STOPWATCH_TIME_CHARACTERISTIC)
  private val statusCharacteristic = Characteristic(uuid, GattProfile.STOPWATCH_STATUS_CHARACTERISTIC)

  val timeStream: Var[Long] = Var(0)

  override def streamTime: Observable[Long] = {
    bleClient.startNotifications(
      timeCharacteristic,
      data =>
        timeStream := data.toSeconds
    )

    timeStream
  }

  override def getStatus: Future[StopWatchStatus] = {
    bleClient
      .read(statusCharacteristic)
      .map(data => data.toStopWatchStatus)
  }

  override def setStatus(status: StopWatchStatus): Future[Boolean] = {
    bleClient
      .write(statusCharacteristic, status.toDataView)
      .map(_ => true)
  }
}
