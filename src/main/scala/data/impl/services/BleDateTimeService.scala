package cz.cvut.fit.psl.vizeldim
package data.impl.services

import ble.{BleClient, Characteristic}
import data.impl.GattProfile
import data.impl.domain.DataViewConverterExtensions.toClockDateTime
import data.layer.domain.ClockDateTime
import data.layer.services.DateTimeService

import monix.reactive.Observable
import monix.reactive.subjects.Var
import monix.execution.Scheduler.Implicits.global

class BleDateTimeService(private val bleClient: BleClient) extends DateTimeService {
  private val uuid = GattProfile.CLOCK_SERVICE_UUID
  private val dateTimeCharacteristic = Characteristic(uuid, GattProfile.DATE_TIME_CHARACTERISTIC)

  val dateTimeStream: Var[ClockDateTime] = Var(ClockDateTime())

  override def streamDateTime: Observable[ClockDateTime] = {
    bleClient.startNotifications(dateTimeCharacteristic, data => {
      dateTimeStream := data.toClockDateTime
    })

    dateTimeStream
  }
}
