package cz.cvut.fit.psl.vizeldim
package data.layer.services

import data.layer.domain.StopWatchStatus

import monix.reactive.Observable

import scala.concurrent.Future

trait StopWatchService {
  def streamTime: Observable[Long]
  def getStatus: Future[StopWatchStatus]
  def setStatus(status:StopWatchStatus): Future[Boolean]
}
