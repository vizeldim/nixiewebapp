package cz.cvut.fit.psl.vizeldim
package data.layer.services

import data.layer.domain.ConnectionStatus

import monix.reactive.Observable

import scala.concurrent.Future

trait ConnectionService {
  def scan(): Future[String]

  def connect(deviceId: String): Future[Boolean]
  
  def streamConnectionStatus(): Observable[ConnectionStatus]
}
