package cz.cvut.fit.psl.vizeldim
package data.layer.services

import data.layer.domain.{TimerStatus}

import monix.reactive.Observable

import scala.concurrent.Future

trait TimerService {
  def streamTime: Observable[Long]
  def setStartTime(seconds: Long): Future[Boolean]
  def streamStatus: Observable[TimerStatus]
  def setStatus(status: TimerStatus): Future[Boolean]
  def readStatus(): Future[TimerStatus]
}
