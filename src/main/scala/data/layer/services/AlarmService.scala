package cz.cvut.fit.psl.vizeldim
package data.layer.services

import data.layer.domain.{Alarm, AlarmTime}

import scala.concurrent.Future

trait AlarmService {
  def getById(id: Int): Future[Alarm]
  def setStatus(id: Int, isActive: Boolean): Future[Boolean]
  def setTime(id: Int, time: AlarmTime): Future[Boolean]
}
