package cz.cvut.fit.psl.vizeldim
package data.layer.services

import data.layer.domain.ClockDateTime

import monix.reactive.Observable

trait DateTimeService {
  def streamDateTime: Observable[ClockDateTime]
}
