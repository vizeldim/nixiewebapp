package cz.cvut.fit.psl.vizeldim
package data.layer.domain

enum TimerStatus {
  case Paused
  case Running
  case Reset
}