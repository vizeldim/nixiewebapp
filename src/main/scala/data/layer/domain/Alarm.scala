package cz.cvut.fit.psl.vizeldim
package data.layer.domain

case class Alarm(id: Int, time: AlarmTime, isActive: Boolean)

case class AlarmTime(hour: Int, minute: Int)
