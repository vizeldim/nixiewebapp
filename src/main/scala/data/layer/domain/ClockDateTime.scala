package cz.cvut.fit.psl.vizeldim
package data.layer.domain

case class ClockDateTime(time: ClockTime = ClockTime(),
                         date: ClockDate = ClockDate())

case class ClockDate(day: Int = 0,
                    month: Int = 0,
                    year: Int = 0)

case class ClockTime(hour: Int = 0,
                     minute: Int = 0,
                     seconds: Int = 0)
