package cz.cvut.fit.psl.vizeldim
package data.layer.domain

enum StopWatchStatus {
  case Paused
  case Running
  case Reset
}