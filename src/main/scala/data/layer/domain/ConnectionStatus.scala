package cz.cvut.fit.psl.vizeldim
package data.layer.domain
enum ConnectionStatus {
  case Connected
  case Connecting
  case Disconnected
}
