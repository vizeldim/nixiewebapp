package cz.cvut.fit.psl.vizeldim
package data.stub.services

import data.impl.domain.DataViewConverterExtensions.toClockDateTime
import data.layer.domain.ClockDateTime
import data.layer.services.DateTimeService

import monix.reactive.Observable

import scala.concurrent.duration.*
import scala.scalajs.js
import scala.scalajs.js.timers.clearTimeout
import monix.execution.Scheduler.Implicits.global

class TestDateTimeService extends DateTimeService {
  private val dateTimeObservable: Observable[ClockDateTime] = Observable.intervalAtFixedRate(1.second).map { _ =>
      val dt = new js.Date(js.Date.now() - this.offset)
      val clockDateTime: ClockDateTime = dt.toClockDateTime

      clockDateTime
    }

  private val offset: Int = 0

  override def streamDateTime: Observable[ClockDateTime] = {
    dateTimeObservable
  }
}
