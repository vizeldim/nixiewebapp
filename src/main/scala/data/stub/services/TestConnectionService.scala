package cz.cvut.fit.psl.vizeldim
package data.stub.services
import ble.BleClient
import data.impl.GattProfile
import data.layer.domain.ConnectionStatus
import data.layer.services.ConnectionService
import data.stub.utils.uDelay

import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import monix.reactive.subjects.Var

import scala.concurrent.Future

class TestConnectionService extends ConnectionService {
  private val connectionStatus: Var[ConnectionStatus] = Var(ConnectionStatus.Disconnected)

  // returns deviceId
  override def scan(): Future[String] = {
    uDelay(2000, "deviceID")
  }

  override def connect(deviceId: String): Future[Boolean] = {
    connectionStatus := ConnectionStatus.Connecting
    uDelay(1000, true, () => connectionStatus := ConnectionStatus.Connected)
  }

  override def streamConnectionStatus(): Observable[ConnectionStatus] = { connectionStatus }
}