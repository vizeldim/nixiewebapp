package cz.cvut.fit.psl.vizeldim
package data.stub.services

import data.layer.domain.TimerStatus
import data.layer.services.TimerService

import monix.reactive.Observable

import scala.concurrent.Future
import monix.eval.Task
import monix.reactive.Observable
import monix.reactive.subjects.Var

import scala.concurrent.duration.*
import scala.scalajs.js
import concurrent.ExecutionContext.Implicits.global
import monix.execution.Scheduler.Implicits.global
class TestTimerService extends TimerService {
  private var tStartTime = js.Date.now().toLong
  private val tCurrentStatus = Var(TimerStatus.Reset)
  private var tStopTime: Int = 0
  private var tInitSeconds: Long = 0

  private val timeObservable: Observable[Long] = Observable.intervalAtFixedRate(500.milliseconds).map { _ =>
      tCurrentStatus() match {
        case TimerStatus.Running =>
          val curr = (tInitSeconds - ((js.Date.now() - tStartTime) / 1000).toLong).toInt
          if (curr < 1) {
            tCurrentStatus := TimerStatus.Reset
            0
          } else {
            curr
          }
        case TimerStatus.Paused => tStopTime
        case TimerStatus.Reset => 0
      }
    }

  override def streamTime: Observable[Long] = timeObservable

  override def streamStatus: Observable[TimerStatus] = tCurrentStatus

  override def setStartTime(seconds: Long): Future[Boolean] = {
    tInitSeconds = seconds
    Future(true)
  }


  override def setStatus(status: TimerStatus): Future[Boolean] = {
    val prevStatus = tCurrentStatus()
    tCurrentStatus := status

    val now = js.Date.now().toLong
    if (status == TimerStatus.Paused) {
      tStopTime = prevStatus match {
        case TimerStatus.Running => (tInitSeconds - ((now - tStartTime) / 1000)).toInt
        case TimerStatus.Reset => 0
        case _ => tStopTime
      }
    } else if (status == TimerStatus.Running) {
      if (prevStatus == TimerStatus.Reset) {
        tStartTime = now
      } else if (prevStatus == TimerStatus.Paused) {
        tStartTime = now - ((tInitSeconds - tStopTime).toLong * 1000)
      }
    }
    Future(true)
  }

  override def readStatus(): Future[TimerStatus] = Future(tCurrentStatus())
}


