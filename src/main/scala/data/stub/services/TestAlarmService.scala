package cz.cvut.fit.psl.vizeldim
package data.stub.services

import ble.{BleClient, Characteristic}
import data.impl.GattProfile
import data.impl.domain.DataViewConverterExtensions.{toAlarmTime, toBoolean, toDataView}
import data.layer.domain.{Alarm, AlarmTime}
import data.layer.services.AlarmService
import data.stub.utils.uDelay

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.scalajs.js


class TestAlarmService() extends AlarmService {
  private var alarmList = (0 to 9).map {
    id => Alarm(id, time = AlarmTime(0, 0), isActive = false)
  }.toList

  // ID: <0, 9>
  override def getById(id: Int): Future[Alarm] = {
    uDelay(500 * id, alarmList(id))
  }

  override def setStatus(id: Int, isActive: Boolean): Future[Boolean] = {
    alarmList = alarmList.updated(id, alarmList(id).copy(isActive = isActive))
    uDelay(1000, true)
  }

  override def setTime(id: Int, time: AlarmTime): Future[Boolean] = {
    alarmList = alarmList.updated(id, alarmList(id).copy(time = time))
    uDelay(1000, true)
  }
}
