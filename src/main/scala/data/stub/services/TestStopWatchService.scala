package cz.cvut.fit.psl.vizeldim
package data.stub.services

import data.layer.domain.StopWatchStatus
import data.layer.services.StopWatchService

import monix.reactive.Observable

import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global
import concurrent.duration.DurationInt
import scala.scalajs.js
import scala.scalajs.js.timers.setInterval

class TestStopWatchService extends StopWatchService {
  private var sStartTime = js.Date.now().toLong
  private var sCurrentStatus =  StopWatchStatus.Reset
  private var sStopTime: Int = 0

  private val timeObservable: Observable[Long] = Observable.intervalAtFixedRate(1.second).map { _ =>
      val t = sStartTime
      val status = sCurrentStatus
      val pauseTime = sStopTime / 1000

      status match {
        case StopWatchStatus.Running =>
          ((js.Date.now() - t) / 1000).toInt
        case StopWatchStatus.Paused =>
          pauseTime
        case StopWatchStatus.Reset =>
          0
      }
  }


  override def streamTime: Observable[Long] = {
    timeObservable
  }

  override def getStatus: Future[StopWatchStatus] = {
    Future(sCurrentStatus)
  }

  override def setStatus(status: StopWatchStatus): Future[Boolean] = {
    val prevState = sCurrentStatus
    sCurrentStatus = status
    val t = sStartTime

    if (status == StopWatchStatus.Paused) {
      if (prevState == StopWatchStatus.Running) {
        sStopTime = (js.Date.now() - t).toInt
      } else {
        sStopTime = 0
      }
    } else if (status == StopWatchStatus.Running) {
      if (prevState == StopWatchStatus.Reset) {
        sStartTime = js.Date.now().toLong
      } else if (prevState == StopWatchStatus.Paused) {
        val pauseTime = sStopTime
        sStartTime = js.Date.now().toLong - pauseTime
      }
    }

    this.sCurrentStatus = status
    Future(true)
  }
}
