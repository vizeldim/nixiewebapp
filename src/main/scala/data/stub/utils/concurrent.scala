package cz.cvut.fit.psl.vizeldim
package data.stub.utils

import scala.concurrent.{Future, Promise}
import scala.scalajs.js
import scala.concurrent.ExecutionContext.Implicits.global

def delay(milliseconds: Int): Future[Unit] = {
  val p = Promise[Unit]()
  js.timers.setTimeout(milliseconds) {
    p.success(())
  }
  p.future
}

def uDelay[T](ms: Int, ret: T, cb: () => Unit = { () => } ): Future[T] = {
  for {
    _ <- delay(ms)
  } yield {
    cb()
    ret
  }
} 
