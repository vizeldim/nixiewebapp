# [NI-PSL] Semestral Project

A web application for Nixie Clock.

Published using gitlab pages: https://vizeldim.gitlab.io/nixiewebapp/

Simplified version of: https://gitlab.com/vizeldim/nixieapp

## Functionality:
- Show current date & time
- Stopwatch (run, pause, reset)
- Timer (pick time, run, pause, reset)
- 10 Alarms (set time, enable/disable)

## Communication

Web App uses Web Bluetooth API to communicate with a Clock GATT Server.

## Technologies && libraries
- MVVM Architecture
- JS Web Bluetooth API was mapped to Scala Facade
- ScalaJS & ScalaTags were used for UI
- Monix Library was used for GATT Notifications (Observable pattern)

## Web App was created following this Figma layout 
https://www.figma.com/file/LVnmcyV5kGA0JcMZXxEsDr/Untitled?node-id=0%3A1

This layout was originally created for the Arduino Clock (which communicated with another web app using HTTP protocol).

Arduino Web Alarm project link: https://gitlab.com/vizeldim/arduino-webalarm/

So the same CSS (from Arduino WebAlarm project) was used, but the final layout was extended to support StopWatch & Timer functionalities and Loading states for each component.



